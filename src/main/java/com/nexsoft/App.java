package com.nexsoft;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {

//        Test Pertama
        Test1 test1 = new Test1();
        System.out.println("Year");
        test1.showCalendarYear(2020);

        System.out.println("Only Months");
        test1.showCalendarMonth("Feb", 2020);

        System.out.println("Only Week");
        test1.showCalendarWeek("Juni", 2, 2020);

//      Test Kedua
//      Parkir Masuk
        Test2 parking = new Test2();
        parking.setVehicle("motor");
        parking.setType("in");
        parking.setDateTimeIn("2003-03-03 19:04:24");
        System.out.println("=== Parkir Masuk ===");
        System.out.println(parking.getParkingId() + "\n");

        // Parkir Keluar
        parking.setType("out");
        parking.setParkingId("P20331");
        parking.setDateTimeOut("2003-03-03 20:04:24");
        System.out.println("=== Parkir Keluar ===");
        System.out.println(parking.countPrice());
    }
}