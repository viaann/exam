package com.nexsoft;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Test2 {
    private String vehicle;
    private String type;
    private String dateTimeIn;
    private String dateTimeOut;
    private String parkingId;
    private LocalDateTime inDateTime;
    private LocalDateTime outDateTime;
    private String parkingIdIn;
    private int counter = 0;

    public void setParkingId(String parkingId) {
        this.parkingId = parkingId;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDateTimeIn(String dateTime) {
        this.dateTimeIn = dateTime;
    }

    public void setDateTimeOut(String dateTime) {
        this.dateTimeOut = dateTime;
    }


    public String getParkingId() {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        inDateTime = LocalDateTime.parse(getDateTimeIn(), dateTimeFormatter);

        String year = String.valueOf(inDateTime.getYear());
        String month = String.valueOf(inDateTime.getMonthValue());
        String date = String.valueOf(inDateTime.getDayOfMonth());
        this.counter += 1;


        this.parkingIdIn = "Id Parkir: " + "P" + year.substring(0, 2) + month + date + String.valueOf(this.counter);

        return this.parkingIdIn;
    }

    public String countPrice() {
        String result = "";
        int harga = 0;
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        outDateTime = LocalDateTime.parse(getDateTimeOut(), dateTimeFormatter);

        int in = inDateTime.getHour();
        int out = outDateTime.getHour();
        int selisihJam = out - in;

        int dateIn = inDateTime.getDayOfMonth();
        int dateOut = outDateTime.getDayOfMonth();
        int selisihDate = dateOut - dateIn;

        if (inDateTime.getDayOfMonth() != outDateTime.getDayOfMonth()) {
            if (selisihDate <= 1) {
                selisihJam = (24 - in) + out;
            } else {
                selisihJam = (24 - in) + out * selisihDate;
            }
        }

        if (selisihJam >= 1) {
            if (getVehicle().equals("mobil")) {
                harga += 5000;
                harga += 2000 * (selisihJam - 1);
            } else {
                harga += 3000;
                harga += 1000 * (selisihJam - 1);
            }
        }
        result = "Total Parkir: Rp." + harga;
        return result;
    }

    public String getDateTimeIn() {
        return this.dateTimeIn;
    }

    public String getDateTimeOut() {
        return this.dateTimeOut;
    }

    public String getVehicle() {
        return this.vehicle;
    }
}
