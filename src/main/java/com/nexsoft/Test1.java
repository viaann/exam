package com.nexsoft;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.IsoFields;
import java.time.temporal.TemporalAdjusters;
import java.time.temporal.WeekFields;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Test1 {
    String[] months = {
            "",
            "Januari", "Februari", "Maret", "April",
            "Mei", "Juni", "Juli", "Agustus",
            "September",  "Oktober", "November", "Desember"
    };

    int[] totalDays = {
            0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    };

    // function untuk mementukan tanggal akan dibulan di setiap bulannya
    public static int day(int month, int day, int year) {
        int y = year - (14 - month) / 12;
        int x = y + y/4 - y/100 + y/400;
        int m = month + 12 * ((14 - month) / 12) - 2;
        int d = (day + x + (31*m)/12) % 7;
        return d;
    }

    public void showCalendarYear(int year) {
        for (int i = 1; i <= 12; i++) {
            System.out.println("=== Bulan " + months[i] + " ===");
            System.out.println(" M  S  S  R  K  J  S");
            if  ((((year % 4 == 0) && (year % 100 != 0)) ||  (year % 400 == 0)) && i == 2) {
                totalDays[i] = 29;
            }

                int startDay = day(i, 1, year);

                for (int j  = 0; j < startDay; j++) {
                    System.out.print("-- ");
                }

                for (int k = 1; k <= totalDays[i]; k++) {
                    System.out.printf("%3d", k);
                    if (((k + startDay) % 7 == 0) || (k == totalDays[i])) System.out.println();
                }
            System.out.println("====================");
            System.out.println();
        }
    }


    public void showCalendarMonth(String month, int year) {
        Date date = null;
        try {
            // untuk mengambil angka dari inputan bulan
             date = new SimpleDateFormat("MMM", Locale.ENGLISH).parse(month);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int monthInt = cal.get(Calendar.MONTH) + 1;
        System.out.println("=== Bulan " + months[monthInt] + " ===");
        System.out.println(" M  S  S  R  K  J  S");

        if  ((((year % 4 == 0) && (year % 100 != 0)) ||  (year % 400 == 0)) && monthInt == 2) {
            totalDays[monthInt] = 29;
        }

        int startDay = day(monthInt, 1, year);

        for (int j  = 0; j < startDay; j++) {
            System.out.print("-- ");
        }

        for (int k = 1; k <= totalDays[monthInt]; k++) {
            System.out.printf("%3d", k);
            if (((k + startDay) % 7 == 0) || (k == totalDays[monthInt])) {
                System.out.println();
            }
        }
        System.out.println("====================");
    }



    public void showCalendarWeek(String month, int week, int year) {
        // untuk mengambil angka dari inputan bulan
        Date date = null;
        try {
            date = new SimpleDateFormat("MMM", Locale.ENGLISH).parse(month);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal = Calendar.getInstance();
        int monthInt = cal.get(Calendar.MONTH) + 1;
        int startWeekDate = getWeekStartDate(week, monthInt, year);
        int endWeekDate = getWeekEndDate(week, monthInt, year);

        System.out.println("--------------");
        System.out.println(" M  S  S  R  K  J  S");

        for (int i = startWeekDate; i <= endWeekDate; i++) {
            System.out.printf("%3d", i);
        }
        System.out.println();
        System.out.println("--------------");


    }

    public static int getWeekStartDate(int week, int month, int year) {
        Calendar calendar = Calendar.getInstance();
        while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.WEEK_OF_MONTH, week);
            calendar.add(Calendar.DATE, -1);
        }
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    public static int getWeekEndDate(int week, int month, int year) {
        Calendar calendar = Calendar.getInstance();

        while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.WEEK_OF_MONTH, week);
            calendar.add(Calendar.DATE, 1);
        }
        calendar.add(Calendar.DATE, -1);
        return calendar.get(Calendar.DAY_OF_MONTH);
    }


}
